using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager :MonoBehaviour {
    [Header("- Score")]
    [SerializeField] TextMeshProUGUI scoreTXT; // UI
    private int score = 0;

    public void addScore(int newScore = 1) {
        score++;
        scoreTXT.text = "Pontos: " + score;
    }
}
