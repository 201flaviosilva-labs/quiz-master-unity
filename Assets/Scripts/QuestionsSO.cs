using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Quiz Question", fileName = "New Question")]
public class QuestionsSO :ScriptableObject {
    [TextArea(2, 6)]
    [SerializeField] string question = "Question?";
    [SerializeField] string[] options = new string[4];
    [SerializeField] int answerIndex;


    public string GetQuestion() {
        return question;
    }

    public int GetAnserIndex() {
        return answerIndex;
    }

    public string GetOptionByIndex(int index) {
        return options[index];
    }
}
