using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Quiz :MonoBehaviour {
    [Header("- Questions")]
    [SerializeField] List<QuestionsSO> questions = new List<QuestionsSO>();
    private List<QuestionsSO> questionsBackUp;
    QuestionsSO currentQuestion;

    [Header("- Question Label UI")]
    [SerializeField] TextMeshProUGUI questionTXT; // UI

    [Header("- Buttons UI")]
    [SerializeField] GameObject[] optionsBTNs; // UI

    [Header("- Buttons Sprites")]
    [SerializeField] Sprite defaultButtonSprite;
    [SerializeField] Sprite answerButtonSprite;

    // Timer Variables
    private bool isAnswerd = false;
    [SerializeField] float secondsGetNextQuestion = 1f;
    private float currentSecondGetNextQuestion = 0f;

    // Score Variables
    private ScoreManager _scoreManager;

    private void Awake() {
        questionsBackUp = new List<QuestionsSO>(questions);
        _scoreManager = FindObjectOfType<ScoreManager>();
    }

    private void Start() {
        GetNextQuestion();
    }

    private void Update() {
        if (isAnswerd) {
            currentSecondGetNextQuestion += Time.deltaTime;
            if (secondsGetNextQuestion <= currentSecondGetNextQuestion) {
                GetNextQuestion();
            }
        }
    }

    private void GetNextQuestion() {
        currentSecondGetNextQuestion = 0;
        isAnswerd = false;
        SetButtonsState(true);
        SetDefaultButtonsSprites();

        // If all questions as been removed
        if (questions.Count <= 0) {
            questions = new List<QuestionsSO>(questionsBackUp);
        }

        SelectRandomQuestion();
        UpdateUIQuestionAndButtons();
    }

    private void UpdateUIQuestionAndButtons() {
        questionTXT.text = currentQuestion.GetQuestion();

        for (int i = 0; i < optionsBTNs.Length; i++) {
            TextMeshProUGUI optionTXT = optionsBTNs[i].GetComponentInChildren<TextMeshProUGUI>();
            optionTXT.text = currentQuestion.GetOptionByIndex(i);
        }
    }

    private void SelectRandomQuestion() {
        int index = Random.Range(0, questions.Count);
        currentQuestion = questions[index];
        if (questions.Contains(currentQuestion)) {
            questions.Remove(currentQuestion);
        }
    }

    public void OnOptionBTNClick(int index) {
        isAnswerd = true;
        ShowAnswer(index);
        SetButtonsState(false);
    }

    private void ShowAnswer(int index) {
        int currentAnswerIndex = currentQuestion.GetAnserIndex();

        if (index == currentAnswerIndex) {
            questionTXT.text = "EST� CERTOOOOOOO!!";

            Image btnImage = optionsBTNs[index].GetComponent<Image>();
            btnImage.sprite = answerButtonSprite;

            _scoreManager.addScore();

        } else {
            questionTXT.text = "Nop: \n" + currentQuestion.GetOptionByIndex(currentAnswerIndex);

            Image btnImage = optionsBTNs[currentAnswerIndex].GetComponent<Image>();
            btnImage.sprite = answerButtonSprite;
        }
    }

    private void SetButtonsState(bool state) {
        for (int i = 0; i < optionsBTNs.Length; i++) {
            Button button = optionsBTNs[i].GetComponent<Button>();
            button.interactable = state;
        }
    }

    private void SetDefaultButtonsSprites() {
        for (int i = 0; i < optionsBTNs.Length; i++) {
            Image btnImage = optionsBTNs[i].GetComponent<Image>();
            btnImage.sprite = defaultButtonSprite;
        }
    }
}
