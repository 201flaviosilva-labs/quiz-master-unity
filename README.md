# Quiz Master

## Description

Jogo baseado no tutorial da GameDev.tv Team na Udemy :)

## Links

- [Play](https://master.d3qmg4fk063k5p.amplifyapp.com/src/Games/QuizMaster/index.html);
- [Repository](https://bitbucket.org/201flaviosilva/quiz-master/);
- [Udemy Curse (Tutorial) And Assets](https://www.udemy.com/course/unitycourse/);
